﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSelectCharacter : MonoBehaviour
{
    [SerializeField] Material altMatJoints;
    [SerializeField] Material altMatBody;
    [SerializeField] SkinnedMeshRenderer rendererJoints;
    [SerializeField] SkinnedMeshRenderer rendererBody;
    Material originalMatJoints;
    Material originalMatBody;

    public void Awake() {
        originalMatJoints = rendererJoints.material;
        originalMatBody = rendererBody.material;
    }

    public void ChangeColour(bool alt) {
        if (alt) {
            rendererJoints.material = altMatJoints;
            rendererBody.material = altMatBody;
        } else {
            rendererJoints.material = originalMatJoints;
            rendererBody.material = originalMatBody;
        }
    }
}
