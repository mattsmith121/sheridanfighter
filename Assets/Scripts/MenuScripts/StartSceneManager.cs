﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartSceneManager : MonoBehaviour
{
    [Header("Camera Fields")]
    [SerializeField] Transform cameraTransform;
    [SerializeField] Transform menuTransform;
    [SerializeField] Transform playerSelectTransform;
    [SerializeField] float cameraLerpSpeed;
    bool lookingAtMenu;

    [Header("Player Select Fields")]
    [SerializeField] PlayerSelectController p1Controller;
    [SerializeField] PlayerSelectController p2Controller;
    [SerializeField] GameObject p1ReadyText, p2ReadyText;
    bool p1Ready, p2Ready;

    void Start() {
        lookingAtMenu = true;
    }

    void Update() {
        if (lookingAtMenu) {
            MenuUpdate();
        }
        else {
            PlayerSelectUpdate();
        }
    }

    void MenuUpdate() {
        cameraTransform.position = Vector3.Lerp(cameraTransform.position, menuTransform.position, cameraLerpSpeed * Time.deltaTime);

        if (Input.GetButtonDown("A1")) {
            OnPlayClick();
        }

        if (Input.GetButtonDown("B1")) {
            OnQuitClick();
        }
    }

    void PlayerSelectUpdate() {
        cameraTransform.position = Vector3.Lerp(cameraTransform.position, playerSelectTransform.position, cameraLerpSpeed * Time.deltaTime);

        // Back button
        if (Input.GetButtonDown("B1")) {
            p1Ready = false;
            p1ReadyText.SetActive(p1Ready);
            p2Ready = false;
            p2ReadyText.SetActive(p2Ready);
            lookingAtMenu = true;
        }

        // Player 1
        if (Input.GetButtonDown("A1")) {
            p1Ready = !p1Ready;
            p1ReadyText.SetActive(p1Ready);
            CheckStart();
        }

        // Player 2
        if (Input.GetButtonDown("A2")) {
            p2Ready = !p2Ready;
            p2ReadyText.SetActive(p2Ready);
            CheckStart();
        }
    }

    void CheckStart() {
        // If both player ready load game scene
        if (p1Ready && p2Ready) {
            // Set data
            PersistentData.GetInstance().SetData(true, p1Controller.GetCharIndex(), p1Controller.GetAltColour());
            PersistentData.GetInstance().SetData(false, p2Controller.GetCharIndex(), p2Controller.GetAltColour());
            SceneManager.LoadScene(1);
        }
    }


    public void OnPlayClick() {
        lookingAtMenu = false;
    }

    public void OnQuitClick() {
        Application.Quit();
    }
}
