﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSelectController : MonoBehaviour
{
    [SerializeField] bool player1;
    [SerializeField] PlayerSelectController otherPlayer;
    [SerializeField] PlayerSelectCharacter xBot;
    [SerializeField] PlayerSelectCharacter yBot;
    [SerializeField] int charIndex;
    [SerializeField] IconController iconController;
    PlayerSelectCharacter activeCharacter;
    string changePosButton;
    string horizontalAxis;
    float oldAxis;
    int posIndex;
    bool altColor;

    // Start is called before the first frame update
    void Start()
    {
        if (player1) {
            changePosButton = "X1";
            horizontalAxis = "Horizontal1";
        } else {
            changePosButton = "X2";
            horizontalAxis = "Horizontal2";
        }

        if (charIndex == 0) {
            SetupCharacter(xBot);
        } else {
            SetupCharacter(yBot);
        }

        posIndex = 1;

        // Update select icons
        iconController.SetPlayerInitialIndex(player1, charIndex);
    }

    // Update is called once per frame
    void Update()
    {
        ChangePosUpdate();
        ChangeCharacterUpdate();
    }

    void ChangePosUpdate() {
        if (Input.GetButtonDown(changePosButton)) {
            // Change pos index, max is 4
            posIndex++;
            if (posIndex > 4) {
                posIndex = 1;
            }

            // Tell animator of active bot
            activeCharacter.GetComponent<Animator>().SetTrigger("Pose" + posIndex);
        }
    }

    void ChangeCharacterUpdate() {
        float axis = Input.GetAxis(horizontalAxis);

        if (axis == 1 && axis != oldAxis) {
            ChangeCharacter(1);
        } else if (axis == -1 && axis != oldAxis) {
            ChangeCharacter(0);
        }

        oldAxis = axis;
    }

    void ChangeCharacter(int index) {
        if (charIndex == index) {
            return;
        }

        charIndex = index;

        if (charIndex == 0) {
            xBot.gameObject.SetActive(true);
            yBot.gameObject.SetActive(false);
            SetupCharacter(xBot);
        } else {
            xBot.gameObject.SetActive(false);
            yBot.gameObject.SetActive(true);
            SetupCharacter(yBot);
        }

        iconController.SetPlayerIndex(player1, charIndex);

        // Update pose
        activeCharacter.GetComponent<Animator>().SetTrigger("Pose" + posIndex);
    }

    void SetupCharacter(PlayerSelectCharacter bot) {
        activeCharacter = bot;
        if (otherPlayer.GetCharIndex() == charIndex) {
            activeCharacter.ChangeColour(true);
            altColor = true;
        } else {
            activeCharacter.ChangeColour(false);
            altColor = false;
        }
    }

    public int GetCharIndex() {
        return charIndex;
    }

    public bool GetAltColour() {
        return altColor;
    }
}
