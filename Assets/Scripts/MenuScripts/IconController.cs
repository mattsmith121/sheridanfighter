﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconController : MonoBehaviour
{
    [SerializeField] IconBorderController xBotBorderController;
    [SerializeField] IconBorderController yBotBorderController;

    public void SetPlayerInitialIndex(bool player1, int index) {
        if (index == 0) {
            xBotBorderController.AddPlayer(player1);
        }
        else {
            yBotBorderController.AddPlayer(player1);
        }
    }

    public void SetPlayerIndex(bool player1, int index) {
        if (index == 0) {
            xBotBorderController.AddPlayer(player1);
            yBotBorderController.RemovePlayer(player1);
        }
        else {
            xBotBorderController.RemovePlayer(player1);
            yBotBorderController.AddPlayer(player1);
        }
    }
}
