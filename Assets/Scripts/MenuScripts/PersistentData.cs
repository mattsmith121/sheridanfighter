﻿public class PersistentData
{

    static int player1Index = 0, player2Index = 1;
    static bool player1AltColor = false, player2AltColor = false;
    static PersistentData instance;
    static int p1Score, p2Score;

    private PersistentData() { }

    public static PersistentData GetInstance() {
        if (instance == null) {
            instance = new PersistentData();
        }

        return instance;
    }

    public void SetData(bool player1, int index, bool altColor) {
        if (player1) {
            player1Index = index;
            player1AltColor = altColor;
            p1Score = 0;
        }
        else {
            player2Index = index;
            player2AltColor = altColor;
            p2Score = 0;
        }
    }

    public int GetIndex(bool player1) {
        if (player1) {
            return player1Index;
        }

        return player2Index;
    }

    public bool GetAltColor(bool player1) {
        if (player1) {
            return player1AltColor;
        }

        return player2AltColor;
    }

    public int GetP1Score() {
        return p1Score;
    }

    public int GetP2Score() {
        return p2Score;
    }

    public int Win(bool player1) {
        if (player1) {
            p1Score++;
            return p1Score;
        }

        p2Score++;
        return p2Score;
    }

    public void ResetScores() {
        p1Score = 0;
        p2Score = 0;
    }
}
