﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconBorderController : MonoBehaviour
{
    [SerializeField] Image outer, inner;
    bool player1Here = false, player2Here = false;
    bool innerActive, outerActive = false;

    public void RemovePlayer(bool player1) {
        if (player1) {
            player1Here = false;
        } else {
            player2Here = false;
        }

        if (outerActive) {
            outerActive = false;
            outer.enabled = false;
            if (player1Here) {
                inner.color = Color.red;
            } else {
                inner.color = Color.blue;
            }
        } else if (innerActive) {
            innerActive = false;
            inner.enabled = false;
        }
    }

    public void AddPlayer(bool player1) {
        bool otherPlayer;
        Color color;
        if (player1) {
            player1Here = true;
            otherPlayer = player2Here;
            color = Color.red;
        } else {
            player2Here = true;
            otherPlayer = player1Here;
            color = Color.blue;
        }

        // is other player already here?
        if (otherPlayer) {
            outerActive = true;
            outer.enabled = true;
            outer.color = color;
        } else {
            innerActive = true;
            inner.enabled = true;
            inner.color = color;
        }
    }
}
