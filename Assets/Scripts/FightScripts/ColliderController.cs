﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderController : MonoBehaviour
{
    [SerializeField] Collider coll;
    [SerializeField] int damage;
    [SerializeField] AudioClip[] hitAudio;
    [SerializeField] AudioClip missAudio;
    AudioSource audioSource;

    void Start() {
        // Audio Source is in root
        audioSource = GetComponentInParent<AudioSource>();
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.name == "mixamorig:RightForeArm" || other.gameObject.name == "mixamorig:RightLeg") {
            audioSource.PlayOneShot(missAudio);
            coll.enabled = false;
        }
        else if (other.gameObject.name == "mixamorig:Spine") {
            audioSource.PlayOneShot(hitAudio[Random.Range(0, hitAudio.Length)]);
            other.gameObject.GetComponent<Hitbox>().DealDamage(damage);
        }
    }

}
