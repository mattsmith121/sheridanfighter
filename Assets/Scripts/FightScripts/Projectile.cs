﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] int damage;
    [SerializeField] AudioClip[] hitAudio;
    [SerializeField] AudioClip missAudio;

    void FixedUpdate()
    {
        // Move along the z-axis
        transform.position += -1 * transform.forward * speed * Time.deltaTime;
    }

    void OnTriggerEnter(Collider other) {
        // if collided with block or wall destroy
        if (other.gameObject.name == "mixamorig:RightForeArm" || 
            other.gameObject.name == "mixamorig:RightLeg") {
            AudioSource.PlayClipAtPoint(missAudio, transform.position);
            Destroy(gameObject);
        } else if ( other.gameObject.GetComponent<Wall>() != null) {
            Destroy(gameObject);
        }
        // if collide with spine, deal 10
        else if (other.gameObject.name == "mixamorig:Spine") {
            AudioSource.PlayClipAtPoint(hitAudio[Random.Range(0, hitAudio.Length)], transform.position);
            other.gameObject.GetComponent<Hitbox>().DealDamage(damage);
            Destroy(gameObject);
        }
    }
}
