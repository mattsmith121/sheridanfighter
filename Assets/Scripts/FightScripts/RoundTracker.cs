﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoundTracker : MonoBehaviour
{
    [SerializeField] Image round1;
    [SerializeField] Image round2;

    public void WinRound(int round) {
        if (round == 1) {
            round1.color = Color.yellow;
        } else if (round == 2) {
            round2.color = Color.yellow;
        }
    }
}
