﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightController : MonoBehaviour
{
    [SerializeField] GameObject projectilePrefab;
    [SerializeField] Transform projectileSpawn;
    [SerializeField] Collider rFist, lFist, foot, block, crouchblock;

    [Header("Audio Clips")]
    [SerializeField] AudioClip[] attackAudio;
    [SerializeField] AudioClip jumpAudio;
    [SerializeField] AudioClip landAudio;
    [SerializeField] AudioClip[] tauntAudio;

    bool player1;
    private float oldPadX, oldPadY;
    Animator animator;
    string bPosfix;
    AudioSource audioSource;

    void Start() {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (animator) {
            GetInput();
        }
    }

    void GetInput() {
        if (Input.GetButtonDown("X" + bPosfix)) {
            animator.SetTrigger("Punch");           
        }

        if (Input.GetButtonDown("RB" + bPosfix)) {
            animator.SetTrigger("Projectile");
        }

        if (Input.GetButtonDown("A" + bPosfix)) {
            animator.SetTrigger("Jump");
            audioSource.PlayOneShot(jumpAudio);
        }

        if (Input.GetButtonDown("Y" + bPosfix)) {
            animator.SetTrigger("Kick");
        }

        bool crouching = Input.GetAxis("Vertical" + bPosfix) < -0.2f;
        animator.SetBool("Crouch", crouching);

        bool blocking = Input.GetButton("B" + bPosfix);
        animator.SetBool("Block", blocking);
        if (crouching) {
            crouchblock.enabled = blocking;
            block.enabled = false;
        } else {
            block.enabled = blocking;
            crouchblock.enabled = false;
        }

        animator.SetFloat("Speed", Input.GetAxis("Horizontal" + bPosfix) * (player1 ? 1 : -1));

        float dPadX = Input.GetAxis("Taunt12" + bPosfix);
        if (dPadX != oldPadX && dPadX != 0) {
            if (dPadX == 1) {
                audioSource.PlayOneShot(tauntAudio[0]);
                animator.SetTrigger("Taunt1");
            } else { // not 1 and not 0 => -1
                audioSource.PlayOneShot(tauntAudio[2]);
                animator.SetTrigger("Taunt2");
            }
            
        }
        oldPadX = dPadX;

        float dPadY = Input.GetAxis("Taunt34" + bPosfix);
        if (dPadY != oldPadY && dPadY != 0) {
            if (dPadY == 1) {
                audioSource.PlayOneShot(tauntAudio[1]);
                animator.SetTrigger("Taunt3");
            }
            else { // not 1 and not 0 => -1
                audioSource.PlayOneShot(tauntAudio[3]);
                animator.SetTrigger("Taunt4");
            }

        }
        oldPadY = dPadY;
    }

    public void PlayKickSound() {
        audioSource.PlayOneShot(attackAudio[Random.Range(0, attackAudio.Length)]);
    }

    public void PlayProjectileSound() {
        audioSource.PlayOneShot(attackAudio[Random.Range(0, attackAudio.Length)]);
    }

    public void PlayPunchSound() {
        audioSource.PlayOneShot(attackAudio[Random.Range(0, attackAudio.Length)]);
    }

    public void StartPunch(int left) {
        if (left == 0) {
            lFist.enabled = true;
        }
        else {
            rFist.enabled = true;
        }
    }

    public void EndPunch(int left) {
        if (left == 0) {
            lFist.enabled = false;
        } else {
            rFist.enabled = false;
        }
    }

    public void StartKick() {
        foot.enabled = true;
    }

    public void EndKick() {
        foot.enabled = false;
    }

    public void SetPlayer1(bool player1) {
        this.player1 = player1;
        if (player1) {
            bPosfix = "1";
        } else {
            bPosfix = "2";
        }
    }

    public void FireProjectile() {
        Quaternion rotation = player1 ? Quaternion.Euler(0f, -90f, 0f) : Quaternion.Euler(0f, 90f, 0f);
        GameObject.Instantiate(projectilePrefab, projectileSpawn.position, rotation);
    }

    public void PlayLandSound() {
        audioSource.PlayOneShot(landAudio);
    }
}
