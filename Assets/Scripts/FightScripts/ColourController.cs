﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColourController : MonoBehaviour
{
    [SerializeField] SkinnedMeshRenderer jointRenderer;
    [SerializeField] SkinnedMeshRenderer bodyRenderer;
    [SerializeField] Material altMatJoint;
    [SerializeField] Material altMatBody;

    public void UseAltColour() {
        jointRenderer.material = altMatJoint;
        bodyRenderer.material = altMatBody;
    }

}
