﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] CameraController mainCamera;
    [SerializeField] GameObject xBotPrefab;
    [SerializeField] GameObject yBotPrefab;
    [SerializeField] GameObject endGameScreen, endInstructions;
    [SerializeField] TextMeshProUGUI winnerText;
    [SerializeField] HealthBar player1HealthBar, player2HealthBar;
    [SerializeField] TextMeshProUGUI player1Name, player2Name;
    [SerializeField] TextMeshProUGUI timerText;
    [SerializeField] RoundTracker p1Score, p2Score;
    GameObject player1Obj, player2Obj;
    bool timer, gameOver;
    float time;

    // Start is called before the first frame update
    void Start()
    {
        gameOver = false;
        timer = true;
        time = 99f;

        // Create fighters from data in persistent data
        PersistentData data = PersistentData.GetInstance();
        // Player 1
        SetupCharacter(ref player1Obj, data.GetIndex(true), data.GetAltColor(true), new Vector3(-5f, 0.24f), Quaternion.Euler(0f, 90f, 0f), true);
        p1Score.WinRound(PersistentData.GetInstance().GetP1Score());

        // Player 2
        SetupCharacter(ref player2Obj, data.GetIndex(false), data.GetAltColor(false), new Vector3(5f, 0.24f), Quaternion.Euler(0f, -90f, 0f), false);
        p2Score.WinRound(PersistentData.GetInstance().GetP2Score());
    }

    void SetupCharacter(ref GameObject player, int index, bool altColor, Vector3 spawnPos, Quaternion startRot, bool player1) {
        if (index == 0) {
            player = Instantiate(xBotPrefab, spawnPos, startRot);
        } else {
            player = Instantiate(yBotPrefab, spawnPos, startRot); 
        }

        if (altColor) {
            player.GetComponent<ColourController>().UseAltColour();
        }

        // Setup player data
        Player p = player.GetComponent<Player>();
        if (p == null) {
            Debug.LogError("Character missing player component.");
            return;
        }
        p.SetGameManager(this);

        // Set Name
        if (player1) {
            SetPlayerName(index, player1Name);
        } else {
            SetPlayerName(index, player2Name);
        }

        // Setup fight data
        FightController f = player.GetComponent<FightController>();
        if (f == null) {
            Debug.LogError("Character missing fight controller component.");
            return;
        }
        f.SetPlayer1(player1);

        // Attach to camera
        mainCamera.SetPlayer(player1, player.transform);

        // Attach to healthbar
        if (player1) {
            player1HealthBar.SetPlayer(p);
        } else {
            player2HealthBar.SetPlayer(p);
        }
    }

    void SetPlayerName(int index, TextMeshProUGUI playerName) {
        if (index == 0) {
            playerName.text = "X-bot";
        } else {
            playerName.text = "Y-bot";
        }
    }

    void Update() {
        if (timer) {
            UpdateTime();
        }

        if (gameOver) {
            ProcessEndGameInput();
        }
    }

    void UpdateTime() {
        time -= Time.deltaTime;
        timerText.text = Mathf.CeilToInt(time).ToString();

        if (time <= 0) {
            timer = false;
            GameOver();
        }
    }

    void ProcessEndGameInput() {
        if (Input.GetButtonDown("A1")) {
            PersistentData.GetInstance().ResetScores();
            SceneManager.LoadScene(1);
        }

        if (Input.GetButtonDown("B1")) {
            SceneManager.LoadScene(0);
        }
    }

    public void StopTime() {
        timer = false;
    }

    public void GameOver() {
        string winText;
        float p1Health = player1Obj.GetComponent<Player>().GetHealth();
        float p2Health = player2Obj.GetComponent<Player>().GetHealth();
        if ((p1Health > 0 && p2Health > 0) || (p1Health <= 0 && p2Health <= 0)) {
            // Tie
            winText = "Tie!";
            StartCoroutine(Restart());
        } else if (p1Health <= 0) {
            winText = "Player 2 Wins!";
            int score = PersistentData.GetInstance().Win(false);
            p2Score.WinRound(score);
            if (score >= 2) {
                gameOver = true;
            } else {
                StartCoroutine(Restart());
            }
        } else {
            winText = "Player 1 Wins!";
            int score = PersistentData.GetInstance().Win(true);
            p1Score.WinRound(score);
            if (score >= 2) {
                gameOver = true;
            }
            else {
                StartCoroutine(Restart());
            }
        }

        winnerText.text = winText;
        ShowEndGameScreen();
    }

    void ShowEndGameScreen() {
        endGameScreen.SetActive(true);
        if (gameOver) {
            endInstructions.SetActive(true);
        } else {
            endInstructions.SetActive(false);
        }
    }

    IEnumerator Restart() {
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene(1);
    }

}
