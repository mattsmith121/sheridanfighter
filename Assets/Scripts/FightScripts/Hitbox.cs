﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitbox : MonoBehaviour
{
    [SerializeField] Player player;

    public void DealDamage(int damage) {
        player.TakeDamage(damage);
    }
}
