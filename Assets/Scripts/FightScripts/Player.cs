﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] int health;
    [SerializeField] AudioClip[] takeDamageAudio;
    [SerializeField] AudioClip[] dieAudio;
    GameManager gameManager;
    AudioSource audioSource;

    void Start() {
        audioSource = GetComponent<AudioSource>();
    }

    public void SetGameManager(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    public void TakeDamage(int damage) {
        // If already dead, just return
        if (health <= 0) {
            return;
        }

        // Take damage
        health -= damage;

        // Health less than 0 => big dead
        if (health <= 0) {
            health = 0;
            Die();
        } else {
            // Play damage audio
            audioSource.PlayOneShot(takeDamageAudio[Random.Range(0, takeDamageAudio.Length)]);
            GetComponent<Animator>().SetTrigger("TakeDamage");
        }
    }

    void Die() {
        // Turn off controls
        GetComponent<FightController>().enabled = false;
        // Play Death sound
        audioSource.PlayOneShot(dieAudio[Random.Range(0, dieAudio.Length)]);
        // Play Death animation
        GetComponent<Animator>().SetTrigger("Die");
        // Tell Game Manager to stop time
        gameManager.StopTime();
    }

    // Function to be called by death animation
    // can end game after animation this way
    public void EndGame() {
        // Tell GameManager game is over
        gameManager.GameOver();
    }

    public int GetHealth() {
        return health;
    }
}
