﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] float cameraLerpSpeedX, cameraLerpSpeedZ;
    Transform player1, player2;
    Camera mainCamera;


    void Start() {
        mainCamera = GetComponent<Camera>();
    }

    public void SetPlayer(bool isPlayer1, Transform player) {
        if (isPlayer1) {
            player1 = player;
        } else {
            player2 = player;
        }
    }

    void Update()
    {
        UpdateCameraPosition();
    }

    void UpdateCameraPosition() {
        float distance = Vector3.Distance(player1.position, player2.position);

        // Look at position between players
        float x = distance / 2 + player1.position.x;
        float newX = Mathf.Lerp(mainCamera.transform.position.x, x, Time.deltaTime * cameraLerpSpeedX);

        // Increase z based on distance between players
        // Max distance is 20, Min is 1
        // Max z is -2.5, Min z is -10
        // get value between 0 and 1
        float val = (distance - 1f) / 19f;
        // convert to z range
        float z = val * -7.5f - 2.5f;
        // LERP
        float newZ = Mathf.Lerp(mainCamera.transform.position.z, z, Time.deltaTime * cameraLerpSpeedZ);

        mainCamera.transform.position = new Vector3(newX, mainCamera.transform.position.y, newZ);
    }
}
