﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    Player player;
    [SerializeField] Slider slider;

    public void SetPlayer(Player player) {
        this.player = player;
    }

    public void Update() {
        UpdateHealthBar();
    }

    void UpdateHealthBar() {
        if (player != null) {
            slider.value = 1 - (player.GetHealth() / 100f);
        }
    }
}
